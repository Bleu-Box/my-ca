/**
 * ca.c -- a toroidal (or spherical?) 2d cellular automaton
 * This program works kind of like Conway's Game of Life, except the cells
 * can have more than two states. Each state has different rules regarding the next
 * state to change to. 
 * 
 * The result is a grid with 2 types of ridged structures - one with more horizontal ridges,
 * and another with more vertical ridges that "extrudes" a type of cell very similar to
 * the Conway's Game of Life cells. The "extruded" cells tend to destroy the horizontal
 * structures. 
 */

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int CELL_WIDTH = 1;
const int CELL_HEIGHT = 1;
const int COLS = 500;
const int ROWS = 500;

typedef enum {
  CELLTYPE_DEAD,
  CELLTYPE_HORIZ_A,
  CELLTYPE_HORIZ_B,
  CELLTYPE_DESTROYER,
  CELLTYPE_VERT,
} CellType;

void renderCell(CellType ctype, int x, int y, SDL_Renderer* ren);
CellType doRule(CellType cells[COLS][ROWS], int x, int y);
CellType getCell(CellType cells[COLS][ROWS], int x, int y);
void setCell(CellType cells[COLS][ROWS], int x, int y, CellType ctype);
int nbs(CellType cells[COLS][ROWS], CellType type, int x, int y);

int main() {
  SDL_Window* win = NULL;
  SDL_Renderer* ren;
  SDL_Event event;
  CellType cells[COLS][ROWS];

  ///// initialize SDL /////
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL had an error: %s\n", SDL_GetError());
    return 0;
  }

  SDL_CreateWindowAndRenderer(CELL_WIDTH*COLS, CELL_HEIGHT*ROWS, 0, &win, &ren);

  ///// initialize cells /////
  srand(time(NULL)); // seed the RNG using current time

  // fill the cells with random cells
  for (int x = 0; x < COLS; x++) {
    for(int y = 0; y < ROWS; y++) {
      int choice = rand()%15;
      if (choice < 2) cells[x][y] = CELLTYPE_HORIZ_A;
      else cells[x][y] = CELLTYPE_DEAD;
    }
  } 

  ///// main loop /////
  while (1) {
    // exit main loop when window is closed
    if(SDL_PollEvent(&event) && event.type == SDL_QUIT) break;
    
    // draw and update all of the cells
    CellType newCells[COLS][ROWS];
    for (int x = 0; x < COLS; x++) {
      for(int y = 0; y < ROWS; y++) {
				CellType cState = cells[x][y];
				CellType newState = doRule(cells, x, y);

				// update
				newCells[x][y] = newState;
				// render
				renderCell(cState, x, y, ren);
      }
    } 
    
    SDL_RenderPresent(ren);

    // update `cells`    
    memcpy(cells, newCells, sizeof(cells));
  }
    
  ///// clean up /////
  SDL_DestroyRenderer(ren);
  SDL_DestroyWindow(win);
  SDL_Quit();
  return 0;
}

void renderCell(CellType ctype, int x, int y, SDL_Renderer* ren) {
  SDL_Rect rect;
  rect.x = x*CELL_WIDTH;
  rect.y = y*CELL_HEIGHT;
  rect.w = CELL_WIDTH;
  rect.h = CELL_HEIGHT;

  // set color based on cell type
  switch (ctype) {
  case CELLTYPE_DEAD:
    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
    break;
  case CELLTYPE_HORIZ_A:
    SDL_SetRenderDrawColor(ren, 237, 228, 180, 255);
    break;
  case CELLTYPE_HORIZ_B:
    SDL_SetRenderDrawColor(ren, 200, 0, 0, 255);
    break;
  case CELLTYPE_DESTROYER:
    SDL_SetRenderDrawColor(ren, 173, 0, 150, 255);
    break;
  case CELLTYPE_VERT:
    SDL_SetRenderDrawColor(ren, 100, 0, 0, 255);
    break;
  }

  SDL_RenderFillRect(ren, &rect);
}

CellType doRule(CellType cells[COLS][ROWS], int x, int y) {
  CellType ctype = cells[x][y];
  int horiz_a_nbs = nbs(cells, CELLTYPE_HORIZ_A, x, y);
  int horiz_b_nbs = nbs(cells, CELLTYPE_HORIZ_B, x, y);
  int destroyer_nbs = nbs(cells, CELLTYPE_DESTROYER, x, y);
  int vert_nbs = nbs(cells, CELLTYPE_VERT, x, y);
  int dead_nbs = nbs(cells, CELLTYPE_DEAD, x, y);
  int live_nbs = horiz_a_nbs+horiz_b_nbs+destroyer_nbs+vert_nbs;

  switch (ctype) {
  case CELLTYPE_DEAD:
    if(horiz_b_nbs == 5) return CELLTYPE_HORIZ_B;
    else if(destroyer_nbs == 3) return CELLTYPE_DESTROYER;
    else if(vert_nbs == 3) return CELLTYPE_VERT;
    else if(horiz_a_nbs == 3) return CELLTYPE_HORIZ_A;
    return ctype;
    break;
  case CELLTYPE_HORIZ_A:
    if(cells[(x+COLS-1)%COLS][y] == CELLTYPE_DESTROYER
       || cells[(x+COLS+1)%COLS][y] == CELLTYPE_DESTROYER)
      return CELLTYPE_DEAD;
    if(live_nbs < 2) return CELLTYPE_VERT;
    else if(horiz_a_nbs > 3) return CELLTYPE_HORIZ_B;
    return ctype;
    break;
  case CELLTYPE_HORIZ_B:
    if(cells[x][(y+ROWS-1)%ROWS] == CELLTYPE_HORIZ_A
       || cells[x][(y+ROWS+1)%ROWS] == CELLTYPE_HORIZ_A)
      return ctype;
    else if(horiz_b_nbs < 2 || live_nbs > 3) return CELLTYPE_DEAD;
    return CELLTYPE_HORIZ_A;
    break;
  case CELLTYPE_DESTROYER:
    if(live_nbs < 2) return CELLTYPE_HORIZ_B;
    else if(destroyer_nbs > 3) return CELLTYPE_DEAD;
    return ctype;
    break;
  case CELLTYPE_VERT:
    if(vert_nbs > 3) return CELLTYPE_DEAD;
    else if(live_nbs < 2) return CELLTYPE_DESTROYER;
    return ctype;
    break;
  }
}

int nbs(CellType cells[COLS][ROWS], CellType ctype, int x, int y) {
  int top = (y+ROWS-1)%ROWS;
  int bottom = (y+1)%ROWS;
  int left = (x+COLS-1)%COLS;
  int right = (x+1)%COLS;
  int count = 0;
  
  CellType neighbs[8] = {
    cells[left][top],
    cells[x][top],
    cells[right][top],
    cells[left][y],
    cells[right][y],
    cells[right][bottom],
    cells[x][bottom],
    cells[left][bottom],
  };
  
  for (int i = 0; i < 9; i++) {
    if (neighbs[i] == ctype) count++;
  }

  return count;
}
