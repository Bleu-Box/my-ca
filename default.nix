with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "my-ca";
   buildInputs = [
     gcc
     gnumake
     SDL2
   ];
}
